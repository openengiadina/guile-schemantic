; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests schemantic literal)
  #:use-module (oop goops)
  #:use-module (schemantic rdf)
  #:use-module (schemantic rdf lang-string)
  #:use-module (schemantic xsd)
  #:use-module ((schemantic ns) #:select (rdf xsd))

  #:use-module (srfi srfi-64))

(define-namespace ex "http://example.com/#")

(test-begin "generic literal")

(test-assert "generic literal is a literal"
  (literal?
   (make-generic-literal 1.9 #:datatype (ex "my-datatype"))))

(test-equal
    (ex "my-datatype")
  (literal-datatype
   (make-generic-literal 1.9 #:datatype (ex "my-datatype"))))

(test-end "generic literal")

(test-begin "rdf:langString")

(test-assert "rdf:langString is a literal"
  (literal? (make-lang-string "hello" #:language "en")))

(test-equal "datatype is rdf:langString"
  (rdf "langString")
  (literal-datatype (make-lang-string "hello" #:language "en")))

(test-equal "value is string"
  "hello"
  (literal-value (make-lang-string "hello" #:language "en")))

(test-equal "language can be refed"
  "en"
  (literal-language (make-lang-string "hello" #:language "en")))

(test-end "rdf:langString")

(test-begin "xsd:string")

(test-assert "string is cast to a literal"
    (literal? (make-literal "hello")))

(test-assert "string is cast to a xsd:string"
  (is-a? (make-literal "hello") <xsd:string> ))

(test-equal "datatype is xsd:string"
  (xsd "string") (literal-datatype (make-literal "hello")))

(test-equal "value of xsd:string can be refed"
  "hello"
  (literal-value (make-literal "hello")))

(test-end "xsd:string")

(test-begin "xsd:integer")

(test-assert "integer is cast to a literal"
    (literal? (make-literal 42)))

(test-assert "integer is cast to a xsd:integer"
  (is-a? (make-literal 42) <xsd:integer>))

(test-equal "datatype is xsd:integer"
  (xsd "integer") (literal-datatype (make-literal 42)))

(test-equal "value of xsd:integer can be refed"
  42
  (literal-value (make-literal 42)))

(test-equal "lexical value is string representatoin"
  "42"
  (literal-lexical (make-literal 42)))

(test-end "xsd:integer")
