; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests schemantic iri)
  #:use-module (schemantic iri)
  #:use-module (web uri)
  #:use-module (srfi srfi-64))

(define-namespace ex "http://example.com/#")

(test-begin "iri")

(test-assert "make-iri from string"
    (iri? (make-iri "http://example.com/")))

(test-assert "value of iri is string"
    (equal? "http://example.com/#"
            (iri-value (make-iri "http://example.com/#"))))

(test-assert "make-iri from uri"
    (iri? (make-iri (string->uri "http://example.com/#"))))

(test-assert "value of iri is string of uri"
  (equal? "http://example.com/#"
          (iri-value (make-iri (string->uri "http://example.com/#")))))

(test-assert "make-iri of iri is an iri"
  (iri? (make-iri (make-iri "http://example.com/"))))

(test-equal "equal? compares values"
    (ex "hi") (make-iri "http://example.com/#hi"))

(test-error "make-iri on integer raises an exception"
            (make-iri 4))

(test-end "iri")
