; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests schemantic fragment-graph)
  #:use-module (schemantic rdf)
  #:use-module ((schemantic ns) #:select (rdf))
  #:use-module (schemantic fragment-graph)

  #:use-module (srfi srfi-64))


(define base-subject (make-iri "http://example.com/"))
(define-namespace ex "http://example.com/#")
(define-namespace ex2 "http://example-two.com/#")

(test-begin "fragment-reference")

(test-assert "fragment-reference of self is #f"
  (equal? (make-fragment-reference #f)
          (term->fragment-reference base-subject base-subject)))

(test-assert "can find fragment-reference"
  (equal? (make-fragment-reference "hello")
          (term->fragment-reference base-subject (ex "hello"))))

(test-end "fragment-reference")

(test-begin "fragment-graph")

(test-assert "can make a fragment-graph"
  (fragment-graph?
   (make-fragment-graph base-subject)))

(define my-fg (make-fragment-graph base-subject))

(test-equal "match on empty fragment graph is emtpy"
  '() (graph-match my-fg (make-triple (lvar) (lvar) (lvar))))

(graph-add my-fg (make-triple base-subject (rdf "type") (ex "foo")))

(test-assert "can add a fragment statement"
  (equal?
   (list (make-triple base-subject (rdf "type") (ex "foo")))
   (graph-match my-fg (make-triple (lvar) (lvar) (lvar)))))

(graph-add my-fg (make-triple (ex "bar") (rdf "type") (ex "foo")))

(test-equal "can get statement and fragment statement"
    2 (length
       (graph-match my-fg (make-triple (lvar) (lvar) (lvar)))))

(graph-add my-fg (make-triple (ex "bar") (rdf "type") (ex "foo2")))

(test-equal 3 (length
               (graph-match my-fg (make-triple (lvar) (lvar) (lvar)))))

(graph-match my-fg (make-triple (lvar) (lvar) (lvar)))

(test-equal "can query statement and fragment statement"
  2 (length
     (graph-match my-fg (make-triple (lvar) (lvar) (ex "foo")))))

(test-end "fragment-graph")
