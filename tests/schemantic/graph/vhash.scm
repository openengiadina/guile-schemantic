; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests schemantic graph vhash)
  #:use-module (oop goops)
  #:use-module (schemantic rdf)
  #:use-module (schemantic graph vhash)
  #:use-module ((schemantic ns) #:select (rdf))

  #:use-module (srfi srfi-64))

(define-namespace ex "http://example.com/#")

(test-begin "graph vhash")

(define g (make <vhash-graph>))

(test-assert "vhash-graph is a graph" (graph? g))


;; add an initial triple
(define t1
  (make-triple (ex "foo") (rdf "type") (ex "something")))

(graph-add g t1)

(test-assert "can match single triple"
    (equal? (list t1)
            (graph-match g (lvar) (lvar) (lvar))))

(test-assert "can match single triple by s"
  (equal? (list t1)
          (graph-match g (triple-subject t1) (lvar) (lvar))))

(test-assert "can match single triple by p"
  (equal? (list t1)
          (graph-match g (lvar) (triple-predicate t1) (lvar))))

(test-assert "can match single triple by o"
  (equal? (list t1)
          (graph-match g (lvar) (lvar) (triple-object t1))))

(test-assert "no match"
    (nil? (graph-match g (ex "not-in-graph") (lvar) (lvar))))

;; add another triple
(define t2
  (make-triple (ex "foo") (ex "bar") (make-literal 42)))

(graph-add g t2)

(test-assert "can match second triple"
  (equal? (list t2)
          (graph-match g (ex "foo") (ex "bar") (lvar))))

(test-assert "can match both triples"
  (equal? 2 (length (graph-match g (lvar) (lvar) (lvar)))))

(test-end "graph vhash")
