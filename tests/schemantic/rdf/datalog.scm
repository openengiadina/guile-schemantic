(define-module (tests schemantic rdf datalog)
  #:use-module (schemantic rdf)
  #:use-module ((schemantic ns) #:select (rdf rdfs))
  #:use-module (schemantic serialization turtle)
  #:use-module (schemantic graph vhash)
  #:use-module (schemantic rdf datalog)

  #:use-module (datalog)
  #:use-module (datalog vhash-set)

  #:use-module (ice-9 vlist)
  #:use-module (ice-9 textual-ports)    ; for reading ttl file

  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-171))

(test-begin "Include RDF Graph into Datalog program")

(define clowns (call-with-input-file "tests/data/clowns.ttl" get-string-all))

;; make a new RDF graph and load the turtle file into the graph
(define graph (make-vhash-graph))
(turtle-transduce (tmap identity)
                  (graph-radd graph)
                  clowns
                  (make-iri "urn:base"))

;; Helper to use the ActivityStreams namespace
(define-namespace as "https://www.w3.org/ns/activitystreams#")

;; This program will evaluate to all the ids of Notes attributed to Alice.
(define program
  (list
   (:- (notes (lvar 's))
    (graph (lvar 's) (rdf "type") (as "Note"))
    (graph (lvar 's) (as "attributedTo") (make-iri "https://test.example/alice")))))

(define notes
  (cdr (vhash-assoc 'notes
                    (datalog-eval
                     program
                     #:edb (add-rdf-graph-to-edb 'graph graph vlist-null)))))

(test-eq "exactly one note attribtued to Alice" 1 (set-size notes))

(test-assert "Correct note is in results"
    (set-contains? notes (vector (make-iri "https://test.example/notes/1"))))

(test-end)

(test-begin "ρdf fragment (Simple and Efficient Minimal RDFS)")

(define foods-ttl (make-vhash-graph))

(turtle-transduce
 (tmap identity)
 (graph-radd foods-ttl)
 (call-with-input-file "tests/data/foods.ttl" get-string-all)
 (make-iri "urn:base-iri"))

(define-namespace foods "http://example.org/foods#")

(define foods-rhodf
  (cdr
   (vhash-assoc 'rhodf
                (datalog-eval
                 (append
                  (list
                   ;; Define all triples as rhodf triples.
                   (:- (rhodf (lvar 's) (lvar 'p) (lvar 'o)) (graph-edb (lvar 's) (lvar 'p) (lvar 'o))))
                  ;; Add the Datalog clauses that define rhodf
                  rhodf)
                 #:edb (add-rdf-graph-to-edb 'graph-edb foods-ttl vlist-null)))))

(test-assert "Subclass (b) (Margherita is Food)"
  (set-contains?
   foods-rhodf
   (vector (foods "Margherita") (rdf "type") (foods "Food"))))

(test-assert "Typing (a)"
  (set-contains?
   foods-rhodf
   (vector (foods "Beer") (rdf "type") (foods "Beverage"))))

(test-end)
