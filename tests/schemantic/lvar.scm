; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests schemantic lvar)
  #:use-module (oop goops)

  #:use-module (schemantic rdf)

  #:use-module (srfi srfi-64))

(test-begin "lvar")

(test-assert "can create a lvar"
  (lvar? (lvar)))

(test-assert "unnamed lvars are not equal"
  (not (equal? (lvar) (lvar))))

(test-assert "named lvars are equal if they have equal names"
  (equal? (lvar 'a) (lvar 'a)))

(test-assert "named lvars are not equal if they have different names"
  (not (equal? (lvar 'a) (lvar 'b))))

(test-end "lvar")
