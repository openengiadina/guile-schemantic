; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests schemantic serialization turtle)
  #:use-module (schemantic rdf)
  #:use-module (schemantic serialization turtle)

  #:use-module (oop goops)
  #:use-module (schemantic graph vhash)

  #:use-module (ice-9 textual-ports)    ; for reading ttl file

  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-171))


(define clowns (call-with-input-file "tests/data/clowns.ttl" get-string-all))

(test-begin "turtle")

(test-equal "transduce 10 triples into list"
  10 (length (turtle-transduce (tmap identity)
                               rcons
                               clowns (make-iri "urn:base"))))


(test-equal "transduce into graph"
  10
  (begin
    (define g (make <vhash-graph>))
    (turtle-transduce (tmap identity)
                      (graph-radd g)
                      clowns
                      (make-iri "urn:base"))
    (length (graph-match g (lvar) (lvar) (lvar)))))

(test-end "turtle")
