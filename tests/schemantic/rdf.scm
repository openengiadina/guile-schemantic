; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests schemantic rdf)
  #:use-module (schemantic rdf)
  #:use-module ((schemantic ns) #:select (rdf))
  #:use-module (schemantic graph vhash)
  #:use-module (schemantic serialization turtle)

  #:use-module (srfi srfi-64)
  #:use-module (srfi srfi-171)

  #:use-module (ice-9 textual-ports))

(define g (make-vhash-graph))

(define clowns (call-with-input-file "tests/data/clowns.ttl" get-string-all))

(turtle-transduce (tmap identity)
                  (graph-radd g)
                  clowns
                  (make-iri "urn:base"))

(test-begin "graph-query")

(test-assert "graph-query on graph returns graph-query"
  (graph-query? (graph-query g (make-iri "https://test.example/alice"))))

(test-equal "graph-match on graph-query"
  1 (length
     (graph-match
      (graph-query (graph-query g (make-iri "https://test.example/alice"))
                   (rdf "type")))))

(test-end "graph-query")
