; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests datalog)
  #:use-module (schemantic lvar)
  #:use-module (datalog)
  #:use-module (datalog vhash-set)

  #:use-module (ice-9 vlist)

  #:use-module (srfi srfi-43)
  #:use-module (srfi srfi-64))

(test-begin "datalog")

(test-assert "creat a clause"
  (clause?
   (:- (p (lvar 'x)) (q (lvar 'x)))))

(test-assert "create a fact"
  (fact?
   (! (p 2))))

(test-assert "fact is a clause"
  (clause?
   (! (p 2))))

;; Example

;; A Datalog program that computes reachability (path) in a graph.
(define graph-program
  (list
   (:- (path (lvar 'x) (lvar 'y)) (path (lvar 'x) (lvar 'z)) (edge (lvar 'z) (lvar 'y)))
   (:- (path (lvar 'x) (lvar 'y)) (edge (lvar 'x) (lvar 'y)))
   (! (edge 0 1))
   (! (edge 1 2))
   (! (edge 1 3))
   (! (edge 4 5))
   (! (edge 5 6))))

(define paths
  (cdr (vhash-assoc 'path (datalog-eval graph-program))))

(test-assert "Graph has 8 paths"
  (eq? 8 (set-size paths)))

(test-assert "All edges are a path"
  (and
   (set-contains? paths (vector 0 1))
   (set-contains? paths (vector 1 2))
   (set-contains? paths (vector 1 3))
   (set-contains? paths (vector 4 5))
   (set-contains? paths (vector 5 6))))

(test-assert "Node 3 is reachable from 0"
  (set-contains? paths (vector 0 3)))

(test-end "datalog")
