; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (tests datalog relational-algebra)
  #:use-module (schemantic lvar)
  #:use-module (datalog relational-algebra)
  #:use-module (datalog vhash-set)

  #:use-module (ice-9 vlist)

  #:use-module (srfi srfi-43)
  #:use-module (srfi srfi-64))

(test-begin "relational-algebra")

(test-assert "make-relation"
  (relation?
   (make-relation 'p (lvar 'x) (lvar 'y))))

(test-assert "get attributes from relation"
  (equal? (vector (lvar 'x) (lvar 'y))
          (attributes
           (make-relation 'p (lvar 'x) (lvar 'y)))))


;; Relational Algebra expressions


;; in-memory-relations for testing
(define rel-p
  (make-relation 'p (lvar 'x) (lvar 'y)))

(define rel-q
  (make-relation 'q (lvar 'a) (lvar 'b)))

(define context
  (alist->vhash
   (list
    (cons 'p  (const (set #(0 1) #(1 2) #(2 3) #(3 4) #(4 0)
                          #(0 2)
                          #(2 2))))
    (cons 'q (const (set #(0 "hi") #(1 "how") #(2 "are") #(3 "you") #(4 "?")))))))

(test-assert "evaluate relation"
  (eq? 7
       (set-size
        (evaluate context rel-p))))

;; equality selection

(test-assert "selection does not change attributes")
(equal?
 (attributes rel-p))
(attributes
 (make-equality-selection (lvar 'x) 0 rel-p))

(test-assert "equality-selection with lvar and constant"
  (vlist-null?
   (set-difference
    (set #(0 1) #(0 2))
    (evaluate context
              (make-equality-selection (lvar 'x) 0 rel-p)))))

(test-assert "equality-selection with two lvars"
  (equal?
   (set #(2 2))
   (evaluate context
             (make-equality-selection (lvar 'x) (lvar 'y)
                                      rel-p))))

;; Projection

(test-assert "project to a single attribute"
  (set-empty?
   (set-difference
    (set-fold (lambda (tuple result)
                (set-adjoin result (vector (vector-ref tuple 0))))
              vlist-null
              (evaluate context rel-p))
    (evaluate context
              (make-projection (vector (lvar 'x)) rel-p)))))

(test-assert "project to two attributes (reverse order)"
  (vlist-null?
   (set-difference
    (set-fold (lambda (tuple result)
                (set-adjoin result (list->vector (reverse (vector->list tuple)))))
              vlist-null
              (evaluate context rel-p))
    (evaluate context
              (make-projection (vector (lvar 'y) (lvar 'x)) rel-p)))))

;; Cartesian product

(test-assert "cartesian product returns concatenated attributes"
  (equal?
   (vector-append (attributes rel-p)
                  (attributes rel-q))
   (attributes (make-cartesian-product rel-p rel-q))))


(test-assert "cartesian product has the right number of tuples"
  (equal?

   (* (set-size (evaluate context rel-p))
      (set-size (evaluate context rel-q)))

   (set-size
    (evaluate context
              (make-cartesian-product rel-p rel-q)))))

;; union

(test-assert "union evaluates to set-union"
  (eq? (+ (set-size (evaluate context rel-p))
          (set-size (evaluate context rel-q)))
       (set-size
        (evaluate context (make-union rel-p rel-q)))))


;; built-in: equality

(define eq-context
  (alist->vhash
   (list
    (cons '= (lambda (attributes)
               (set (make-vector
                     (vector-length attributes)
                     (vector-ref
                      attributes
                      (vector-index (compose not lvar?) attributes)))))))))

(test-error "attempting to evaluate equality with no bound variables causes error"
            (evaluate eq-context
                      (make-relation '= (lvar 'x) (lvar 'y))))

(test-assert "evaluate equality-relation after binding onew value"
  (equal? (set #(1 1))
          (evaluate eq-context
                    (relation-bind
                     (make-relation '= (lvar 'x) (lvar 'y))
                     (lvar 'x) 1))))

;; push-down

(test-assert "push-down! binds equality-selection to equality-relation"
  (set-contains?
   (evaluate eq-context
             (push-down
              (make-equality-selection (lvar 'x) 42 (make-relation '= (lvar 'x) (lvar 'y)))))
   #(42 42)))

(test-assert "push-down goes past and removes cartesian-product with a single child"
  (set-contains?
   (evaluate eq-context
             (push-down
              (make-equality-selection (lvar 'x) 0
                                       (make-cartesian-product
                                        (make-relation '= (lvar 'x) (lvar 'y))))))
   #(0 0)))

(test-assert "push-down! pushes equality-selection past union"
  (set-contains?
   (evaluate context
             (push-down
              (make-equality-selection (lvar 'x) 0
                                       (make-union
                                        (make-relation '= (lvar 'x) (lvar 'y)) rel-q))))
   #(0 0)))

(test-end "relational-algebra")
