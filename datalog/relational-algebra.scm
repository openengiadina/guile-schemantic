; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (datalog relational-algebra)
  #:use-module (schemantic lvar)
  #:use-module (datalog vhash-set)

  #:use-module (oop goops)

  #:use-module (ice-9 format)
  #:use-module (ice-9 vlist)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:use-module (srfi srfi-43)

  #:export (<expression>
            expression?
            attributes
            attributes-set
            evaluate
            push-down

            <relation>
            relation?
            make-relation
            relation-bind

            <operator>
            operator?
            operator-children

            <equality-selection>
            make-equality-selection

            <projection>
            make-projection

            <cartesian-product>
            make-cartesian-product

            <union>
            make-union))

;; Helpers

(define* (vhash-ref key vhash #:key (default #f))
  (cond
   ((vhash-assoc key vhash) (cdr (vhash-assoc key vhash)))
   (else default)))

;; A poor man's Maybe ... TODO: use SRFI-189
(define (just x)
  (cons 'just x))

(define (nothing)
  (cons 'nothing '()))

(define (just? maybe)
  (eq? (car maybe) 'just))

(define (nothing? maybe)
  (eq? (car maybe) 'nothing))

(define (maybe-ref maybe)
  (cdr maybe))

;; Relational Algebra
;;
;; A relational algebra expression is a tree:
;; - internal nodes are <operator>
;; - leaves are <relation>

(define-class <expression> ())

(define (expression? x)
  (is-a? x <expression>))

(define-method (attributes (expr <expression>))
  #())

(define-method (attributes-set (expr <expression>))
  (apply set (vector->list (attributes expr))))

(define-method (evaluate context (expr <expression>))
  (set))

(define-class <operator> (<expression>)
  ;; sub expressions
  (children #:init-keyword #:children #:accessor operator-children))

(define (operator? x)
  (is-a? x <operator>))

(define-method (operator-set-children (op <operator>) children)
  (let ((clone (shallow-clone op)))
    (slot-set! clone 'children children)
    clone))

;; Built-ins


(define (add-built-ins-to-context context)
  (vhash-cons '= (lambda (attributes)
                   (set (make-vector
                         (vector-length attributes)
                         (vector-ref attributes
                                     (vector-index (compose not lvar?) attributes)))))
              context))

;; Relation

(define-class <relation> (<expression>)
  (predicate-symbol #:init-keyword #:predicate-symbol #:init-value #f #:getter relation-predicate-symbol)
  (attributes #:init-keyword #:attributes #:getter relation-attributes)
  (bindings #:init-keyword #:bindings #:init-value vlist-null #:getter relation-bindings))

(define-method (attributes (rel <relation>))
  (relation-attributes rel))

(define (make-relation predicate-symbol . attributes)
  (make <relation>
    #:predicate-symbol predicate-symbol
    #:attributes (list->vector attributes)))

(define-method (relation-substituted-attributes (rel <relation>))
  (vector-map
   (lambda (_ attribute)
     (vhash-ref attribute
                (relation-bindings rel)
                #:default attribute))
   (attributes rel)))

(define (relation? x)
  (is-a? x <relation>))

(define-method (write (self <relation>) port)
  (format port "<relation ~a [~{~a~^ ~}]>"
          (relation-predicate-symbol self)
          (vector->list (relation-substituted-attributes self))))

(define-method (relation-bind (rel <relation>) (from-lvar <lvar>) to-constant)
  (make <relation>
    #:predicate-symbol (relation-predicate-symbol rel)
    #:attributes (attributes rel)
    #:bindings (vhash-cons from-lvar to-constant (relation-bindings rel))))

(define-method (evaluate context (self <relation>))
  (let ((proc-or-set (vhash-ref (relation-predicate-symbol self)
                                (add-built-ins-to-context context))))
    (if (procedure? proc-or-set)
        (proc-or-set (relation-substituted-attributes self))
        proc-or-set)))

;; Equality selection

(define-class <equality-selection> (<operator>)
  (operand1 #:init-keyword #:operand1 #:getter equality-selection-operand1)
  (operand2 #:init-keyword #:operand2 #:getter equality-selection-operand2))

(define-method (write (self <equality-selection>) port)
  (format port "<equality-selection [~a=~a] ~a>"
          (equality-selection-operand1 self)
          (equality-selection-operand2 self)
          (vector-ref (operator-children self) 0)))

(define (make-equality-selection operand1 operand2 child)
  (make <equality-selection>
    #:operand1 operand1
    #:operand2 operand2
    #:children (vector child)))

(define-method (attributes (op <equality-selection>))
  (attributes (vector-ref (operator-children op) 0)))

(define-method (equality-selection-constant-value (op <equality-selection>))
  (cond
   ((not (lvar? (equality-selection-operand1 op)))
    (just (equality-selection-operand1 op)))

   ((not (lvar? (equality-selection-operand2 op)))
    (just (equality-selection-operand2 op)))

   (else (nothing))))

(define-method (equality-selection-lvars (op <equality-selection>))
  (filter lvar?
              (list (equality-selection-operand1 op)
                    (equality-selection-operand2 op))))

(define-method (evaluate context (op <equality-selection>))
  (let ((sub-expr (vector-ref (operator-children op) 0))
        (attributes (attributes op)))
    (set-filter
     (lambda (t)
       (apply equal?
              (map (lambda (operand)
                     (if (lvar? operand)
                         ;; if operand is a lvar then derefernce attribute from tuple
                         (vector-ref t (vector-index (lambda (a) (equal? operand a)) attributes))
                         ;; else operand is a constant
                         operand))
                   (list (equality-selection-operand1 op)
                         (equality-selection-operand2 op)))))
     (evaluate context sub-expr))))


;; Projection

(define-class <projection> (<operator>)
  (attributes #:init-keyword #:attributes))

(define-method (attributes (rel <projection>))
  (slot-ref rel 'attributes))

(define (make-projection attributes child)
  (make <projection>
    #:attributes attributes
    #:children (vector child)))

(define-method (write (self <projection>) port)
  (format port "<projection [~{~a~^ ~}] ~a>"
          (vector->list (attributes self))
          (vector-ref (operator-children self) 0)))


(define-method (evaluate context (rel <projection>))
  (let* ((sub-expr (vector-ref (operator-children rel) 0))
         (sub-attributes (attributes sub-expr))
         (ref-map (vector-map
                   (lambda (_ projection-attribute)
                     ;; find the position of the projection-attribute in the attributes of the sub-expression
                     (vector-index (lambda (a) (equal? projection-attribute a)) sub-attributes))
                   (attributes rel))))
    (set-fold
     (lambda (tuple result)
       (set-adjoin result
                   (vector-unfold
                    (lambda (i) (vector-ref tuple (vector-ref ref-map i)))
                    (vector-length ref-map))))
     vlist-null
     (evaluate context sub-expr))))

;; Cartesian product

(define-class <cartesian-product> (<operator>))

(define (make-cartesian-product . children)
  (make <cartesian-product> #:children (list->vector children)))

(define-method (write (self <cartesian-product>) port)
  (format port "<cartesian-product [~{~a~^ ~}]~{ ~a~}>"
          (vector->list (attributes self))
          (vector->list (operator-children self))))

(define-method (attributes (op <cartesian-product>))
  (vector-concatenate
   (vector-fold-right
    (lambda (_ result sub-expr)
      (cons (attributes sub-expr) result))
    '()
    (operator-children op))))

;; NOTE this is a naive implementation that is not efficient.
(define (compute-cartesian-product rels)
  (cond
   ((null? rels) rels)

   ((eq? 1 (length rels)) (car rels))

   (else
    (compute-cartesian-product
     (cons (set-fold
            (lambda (tuple1 result)
              (set-fold
               (lambda (tuple2 result)
                 (set-adjoin result (vector-append tuple1 tuple2)))
               result
               (car (cdr rels))))
            vlist-null
            (car rels))
           (drop rels 2))))))

(define-method (evaluate context (rel <cartesian-product>))
  (let* ((sub-exprs (operator-children rel))
         (sub-tuples (vector-map
                      (lambda (_ sub-rel)
                        (evaluate context sub-rel))
                      sub-exprs)))
    (compute-cartesian-product (vector->list sub-tuples))))

;; Partition the children of a cartesian-product into children that have attributes from the attribute-selection and children that do not.
(define-method (operator-partition-children (op <operator>) attribute-selection)
  (let-values (((not-in-selection in-selection)
                (partition
                 (lambda (child)
                   (set-empty?
                    (set-intersection
                     attribute-selection
                     (attributes-set child))))
                 (vector->list (operator-children op)))))
    (values
     (if (null? in-selection)
         #f
         in-selection)
     (if (null? not-in-selection)
         #f
         not-in-selection))))

;; union

(define-class <union> (<operator>))

(define (make-union . children)
  (make <union>
    #:children (list->vector children)))

(define-method (attributes (op <union>))
  (attributes (vector-ref (operator-children op) 0)))

(define-method (write (self <union>) port)
  (format port "<union [~{~a~^ ~}]~{ ~a~}>"
          (vector->list (attributes self))
          (vector->list (operator-children self))))

(define-method (evaluate context (op <union>))
  (vector-fold
   (lambda (_ result child)
     (set-union result (evaluate context child)))
   (set)
   (operator-children op)))

;; Push down

(define-method (push-down (op <operator>))
  (set! (operator-children op)
    (if (vector? (operator-children op))

        (vector-map
         (lambda (_ child) (push-down child))
         (operator-children op))

        (push-down (operator-children op))))
  op)


;; at the bottom of the tree, can not push down further.
(define-method (push-down (rel <relation>))
  rel)

;; NOTE I feel this is an unnecessary source of complexity and GOOPS might not be helping...
(define-method (push-down (op <equality-selection>))
  (let ((child (vector-ref (operator-children op) 0)))
    (cond
     ;; selection and projection commute iff attributes referenced by selection are
     ((is-a? child <projection>)
      (operator-set-children child
                             (push-down (operator-set-children op (operator-children child)))))

     ((is-a? child <cartesian-product>)
      (if (eq? 1 (vector-length (operator-children child)))
          ;; cartesian product has a single child -> the cartesian product can be removed
          (push-down (operator-set-children op (operator-children child)))

          ;; split the cartesian product into operands that have attributes of the equality selection and such that are not affected
          (let-values
              (((cp-selection cp-rest)
                (operator-partition-children child (apply set (equality-selection-lvars op)))))

            (cond

             ;; cartesian product does not contain attributes from equality selection. TODO: this means the equality-selection is not needed and should be remove or an something went wrong...
             ((not cp-selection) op)

             ;; cartesian product can not be split as equaity selection affects all children. TODO: this is a join and can be done more efficiently.
             ((not cp-rest) op)

             (else
              (apply make-cartesian-product
                     (list
                      (push-down (operator-set-children op (vector (apply make-cartesian-product cp-selection))))
                      (apply make-cartesian-product cp-rest))))))))

     ((is-a? child <union>)
      (if (eq? 1 (vector-length (operator-children child)))
          ;; cartesian product has a single child -> the cartesian product can be removed
          (push-down (operator-set-children op (operator-children child)))

          ;; split the cartesian product into operands that have attributes of the equality selection and such that are not affected
          (let-values
              (((cp-selection cp-rest)
                (operator-partition-children child (apply set (equality-selection-lvars op)))))

            (cond

             ;; cartesian product does not contain attributes from equality selection. TODO: this means the equality-selection is not needed and should be remove or an something went wrong...
             ((not cp-selection) op)

             ;; cartesian product can not be split as equaity selection affects all children. TODO: this is a join and can be done more efficiently.
             ((not cp-rest) op)

             (else
              (apply make-union
                     (list
                      (push-down (operator-set-children op (vector (apply make-union cp-selection))))
                      (apply make-union cp-rest))))))))

     ((is-a? child <relation>)
      (if (just? (equality-selection-constant-value op))
          (let ((constant-to-bind (maybe-ref (equality-selection-constant-value op)))
                (lvar-to-bind (car (equality-selection-lvars op))))
            (relation-bind child lvar-to-bind constant-to-bind))))

     ((is-a? child <equality-selection>)
      (operator-set-children op (vector (push-down child))))

     (else (next-method op)))))
