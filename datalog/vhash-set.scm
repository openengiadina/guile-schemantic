; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (datalog vhash-set)
  #:use-module (ice-9 vlist)

  #:use-module (srfi srfi-1)

  #:export (set

            set?
            set-empty?
            set-contains?

            set-adjoin
            set-size

            set-fold
            set-filter

            set->vlist
            vlist->set
            list->set
            set->list

            set-union
            set-difference
            set-intersection))

;; A set implementation that uses VHashes. Inspired by SRFI-113.


(define (set . args)
  (fold
   (lambda (el set) (set-adjoin set el))
   vlist-null
   args))

(define set?
  vhash?)

(define set-empty? vlist-null?)

(define (set-contains? set el)
  (not (not (vhash-assoc el set))))

(define set-size
  vlist-length)

(define (set-adjoin set el)
  (if (set-contains? set el)
      set
      (vhash-cons el '() set)))

(define (set->vlist set)
  (vlist-map car set))

(define (vlist->set vlist)
  (vlist-fold
   (lambda (el set)
     (set-adjoin set el))
   vlist-null
   vlist))

(define set->list
  (compose vlist->list set->vlist))

(define (list->set lst)
  (fold
   (lambda (el set)
     (set-adjoin set el))
   vlist-null
   lst))

(define (set-fold proc init set)
  (vhash-fold
   (lambda (key _ result)
     (proc key result))
   init
   set))

(define (set-filter proc set)
  (set-fold
   (lambda (el result)
     (if (proc el)
         (set-adjoin result el)
         result))
   vlist-null
   set))

(define (set-union set1 set2)
  (set-fold
   (lambda (el result) (set-adjoin result el))
   set1
   set2))

(define (set-difference set1 set2)
  (set-fold
   (lambda (el result)
     (if (set-contains? set2 el)
         result
         (set-adjoin result el)))
   vlist-null
   set1))

(define (set-intersection set1 set2)
  (set-fold
   (lambda (el result)
     (if (and (set-contains? set2 el))
         (set-adjoin result el)
         result))
   vlist-null
   set1))

