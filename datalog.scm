; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (datalog)

  #:use-module (schemantic lvar)
  #:use-module ((datalog relational-algebra) #:prefix ra:)
  #:use-module (datalog vhash-set)

  #:use-module (oop goops)
  #:use-module (ice-9 format)
  #:use-module (ice-9 vlist)

  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-43)

  #:export (<atom>
            atom?
            make-atom
            atom-predicate
            atom-terms

            <clause>
            clause?
            make-clause
            clause-head
            clause-body

            fact?
            make-fact

            :- !

            datalog-eval))

;; Atom

(define-class <atom> ()
  (predicate #:init-keyword #:predicate #:getter atom-predicate)
  (terms #:init-keyword #:terms #:getter atom-terms))

(define-method (write (self <atom>) port)
  (format port "(~a~{ ~a~})"
          (atom-predicate self)
          (if (vector? (atom-terms self))
              (vector->list (atom-terms self))
              (atom-terms self))))

(define (make-atom predicate terms)
  (make <atom> #:predicate predicate #:terms terms))

(define (atom? x)
  (is-a? x <atom>))

;; Clause

(define-class <clause> ()
  (head #:init-keyword #:head #:init-value #nil #:getter clause-head)
  (body #:init-keyword #:body #:init-value #() #:getter clause-body))

(define-method (write (self <clause>) port)
  (cond
   ((vector-empty? (clause-body self))
    (format port "~a." (clause-head self)))

   (else
    (format port "~a :-~{ ~a~}."
            (clause-head self)
            (if (vector? (clause-body self))
                (vector->list (clause-body self))
                (clause-body self))))))

(define (make-clause head body)
  (make <clause> #:head head #:body body))

(define (clause? x)
  (is-a? x <clause>))

;; Fact (a clause without body)

(define (make-fact atom)
  (make <clause> #:head atom))

(define (fact? x)
  (and (clause? x)
       (vector-empty? (clause-body x))))

;; Syntax for creating clauses

(eval-when (expand load eval)
  (define (syntax->atom x)
    (with-syntax
        (((predicate terms ...) x))
      #'(make-atom (quote predicate) (vector terms ...)))))

(define-syntax :-
  (lambda (x)
    (syntax-case x ()
      ((_ head body ...)
       (with-syntax
           ((head-atom (syntax->atom (syntax head)))
            ((body-atoms ...) (map syntax->atom (syntax (body ...)))))
         (syntax
          (make-clause head-atom (vector body-atoms ...))))))))

(define-syntax !
  (lambda (x)
    (syntax-case x ()
      ((_ head)
       (with-syntax
           ((head-atom (syntax->atom (syntax head))))
         (syntax
          (make-fact head-atom)))))))

;; VHash helpers

(define* (vhash-ref key vhash #:key (default #f))
  (cond
   ((vhash-assoc key vhash) (cdr (vhash-assoc key vhash)))
   (else default)))

(define (vhash-map proc vhash)
  (vhash-fold
   (lambda (key value result)
     (vhash-cons
      key (proc key value)
      result))
   vlist-null
   vhash))

;; Substitution
;;
;; A substitution is a VHash from the term to be replaced to the term to be
;; replaced with.

(define-method (substitute (x <atom>) s)
  (make-atom
   (atom-predicate x)
   (vector-map
    (lambda (_ term) (substitute term s))
    (atom-terms x))))

(define-method (substitute (x <clause>) s)
  (make-clause
   (substitute (clause-head x) s)
   (vector-map
    (lambda (_ atom) (substitute atom s))
    (clause-body x))))

(define-method (substitute (x <top>) s)
  (cond
   ((vhash-assoc x s) (cdr (vhash-assoc x s)))
   (else x)))

;; # Algebraic Naive Evaluation
;;
;; The implemented evaluator computes the fixpoint of a system of algebraic equation (relational algebra).
;;
;; Clauses are first translated to relational algebra expressions, formulated as
;; a system of equations and resolved until the solutions sets stabilize (a
;; fixpoint is found).
;;
;; This is a naive implementation with much room for optimization, clean-up and
;; fixing.

;; Transform clause to relational expression

(define (substitute-constants atoms)
  "Returns a substitution that replaces all constants in the atoms with fresh logical variables"
  (vector-fold
   (lambda (i substitution term)
     (if (lvar? (substitute term substitution))
         substitution
         (vhash-cons term (lvar) substitution)))
   vlist-null
   (vector-concatenate
    (vector->list (vector-map
                   (lambda (_ atom) (atom-terms atom))
                   atoms)))))

(define (lvar-occurrences atoms)
  "Finds occurences of lvars in given atoms (vector of atoms) and returns a VHash from lvars to list of positions they appear in"
  (vector-fold
   (lambda (i occurences atom)
     (vector-fold
      (lambda (j occurences term)
        (if (lvar? term)
            (if (vhash-assoc term occurences)
                (vhash-cons term (vlist-cons (cons i j) (cdr (vhash-assoc term occurences)))
                            (vhash-delete term occurences))
                (vhash-cons term (vlist-cons (cons i j) vlist-null)
                            occurences))
            occurences))
      occurences
      (atom-terms atom)))
   vlist-null
   atoms))

(define (substitute-duplicate-lvars! atoms)
  "Replace duplicate lvars in atoms and returns the applied substitution."
  (vhash-fold
   (lambda (term occurences substitution)
     (if (<= 1 (vlist-length occurences))
         ;; more than one occurence,
         (vlist-fold
          (lambda (occ substitution)
            ;; create a fresh variable
            (let ((x (lvar)))
              ;; replace in atom
              (vector-set! (atom-terms (vector-ref atoms (car occ)))
                           (cdr occ)
                           x)
              ;; add the substitution
              (vhash-cons term x substitution)))
          substitution
          ;; don't replace the first occurence
          (vlist-drop occurences 1))

         substitution))
   ;; initialize empty substitution
   vlist-null
   ;; find all occurences of lvars in atoms
   (lvar-occurrences atoms)))

;; The algorithm T from Section 8.3 of "Logic Programming and Databases". It
;; transforms a clause into a logically equivalent clause where the terms in the
;; head clause are all logical variables and every logical variable appears at most
;; once.
(define-method (normalize-head (clause <clause>))
  (let* ((head (clause-head clause))
         ;; substitute all constants in head with fresh variables
         (case-a-substitution (substitute-constants (vector head)))
         ;; apply case-a-substitution to head
         (head* (substitute head case-a-substitution))
         ;; replace duplicates in head*
         (case-b-substitution (substitute-duplicate-lvars! (vector head*))))
    (make-clause
     head*
     (vector-append
      (clause-body clause)
      (list->vector (vlist->list
                     (vlist-map
                      (lambda (s) (make-atom '= (vector (car s) (cdr s))))
                      (vlist-append case-a-substitution case-b-substitution))))))))


(define (clause->relational-expr clause)
  "Transform a Datalog clause to a Relational Algebra expression."
  (let* ( ;; normalize clause to remove constants and duplicate lvars in head
         (clause (normalize-head clause))
         ;; susbstitute constants in body with fresh variables
         (case-a-substitution (substitute-constants (clause-body clause)))
         ;; apply the substitution of constants and get a vector containing all body terms
         (body (clause-body (substitute clause case-a-substitution)))
         ;; replace duplicate lvars in body-terms. note that the body is modified (!).
         (case-b-substitution (substitute-duplicate-lvars! body))
         ;; append the substition
         (substitution (vlist-append case-a-substitution case-b-substitution))
         ;;
         (base-cartesian-product (apply ra:make-cartesian-product
                                        (vector->list (vector-map (lambda (_ atom)
                                                                    (apply ra:make-relation
                                                                           (cons (atom-predicate atom)
                                                                                 (vector->list (atom-terms atom)))))
                                                                  body)))))

    (ra:make-projection
     (atom-terms (clause-head clause))
     (vlist-fold (lambda (s rel)
                   (ra:push-down (ra:make-equality-selection (car s) (cdr s) rel)))
                 base-cartesian-product
                 substitution))))


(define (clauses->relational-expr clauses)
  "Transfrom a VHash of Datalog clauses (keys are predicate symbols and values are VLists of clauses) to a system of Relational Algebra expressions."
  (vhash-map
   (lambda (predicate clauses)
     (apply ra:make-union
            (vlist->list
             (vlist-map (λ (clause)
                          (clause->relational-expr clause))
                        clauses))))
   clauses))

(define (sort-clauses clauses)
  "Sort a list of clauses by predicate symbol of head. Returns a VHash with predicate symbol as keys."
  (fold
   (lambda (clause result)
     (let ((predicate (atom-predicate (clause-head clause))))
       (vhash-cons
        predicate
        (vlist-cons clause
                    (vhash-ref predicate result #:default vlist-null))
        (vhash-delete predicate result))))
   vlist-null
   clauses))

(define (edb->evaluation-context edb clauses)
  "Extend the Extensional Database into a evaluation context that includes empty sets for all intrinsical predicate symbols."
  (fold
   (lambda (clause result)
     (let ((head (clause-head clause)))
       (vhash-cons
        (atom-predicate head)
        (set)
        (vhash-delete (atom-predicate head) result))))
   edb
   clauses))

(define (init-differentials clauses)
  (fold
   (lambda (clause result)
     (let ((head (clause-head clause)))
       (vhash-cons
        (atom-predicate head)
        (set)
        (vhash-delete (atom-predicate head) result))))
   vlist-null
   clauses))

;; Semi-Naive Evaluation (Section 9.1.2 of Logic Programming and Databases)
(define* (datalog-eval clauses #:key (edb vlist-null))
  "Evaluate a Datalog program"
  (let ((relational-exprs (clauses->relational-expr (sort-clauses clauses))))

    (let loop ((context (edb->evaluation-context edb clauses))
               (differentials (init-differentials clauses)))

      (let ((context+differentials+changes?
             (vhash-fold (λ (predicate relational-expr context+differentials+changes?)

                           (let* ((context (car context+differentials+changes?))
                                  (differentials (cadr context+differentials+changes?))
                                  (old-changes? (cddr context+differentials+changes?))

                                  ;; Create a context that uses the last differential at predicate currently being evaluated
                                  (context-with-differential
                                   (vhash-cons predicate (vhash-ref predicate differentials #:default (set)) (vhash-delete predicate context)))

                                  ;; evaluate the relational expression
                                  (evaluated (ra:evaluate context-with-differential relational-expr))

                                  (new-differential (set-difference evaluated (vhash-ref predicate context)))

                                  (new-values (set-union (vhash-ref predicate context) new-differential))

                                  (changes? (not (set-empty? new-differential))))


                             (cons (vhash-cons predicate new-values (vhash-delete predicate context))
                                   (cons (vhash-cons predicate new-differential differentials) (or changes? old-changes?)))))

                         (cons context (cons differentials #f))
                         relational-exprs)))

        (if (cddr context+differentials+changes?)
            (loop (car context+differentials+changes?)
                  (cadr context+differentials+changes?))
            context)))))
