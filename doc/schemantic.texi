
\input texinfo
@c -*-texinfo-*-

@c %**start of header
@setfilename guile-schemantic.info
@documentencoding UTF-8
@settitle Guile Schemantic Reference Manual
@c %**end of header

@include version.texi

@copying
Copyright @copyright{} 2020 pukkamustard

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3 or
any later version published by the Free Software Foundation; with no
Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
copy of the license is included in the section entitled ``GNU Free
Documentation License''.
@end copying

@dircategory The Algorithmic Language Scheme
@direntry
* Guile Schemantic: (guile-schemantic).   
@end direntry

@titlepage
@title The Guile Schemantic Manual
@author pukkamustard

@page
@vskip 0pt plus 1filll
Edition @value{EDITION} @*
@value{UPDATED} @*

@insertcopying
@end titlepage

@contents

@c *********************************************************************
@node Top
@top Guile Schemantic

This document describes Guile Schemantic version @value{VERSION}.

@menu
* Introduction::                Why Guile Schemantic?
@end menu

@c *********************************************************************
@node Introduction
@chapter Introduction

INTRODUCTION HERE

This documentation is a stub.

@bye
