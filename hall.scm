(hall-description
  (name "schemantic")
  (prefix "guile")
  (version "0.1")
  (author "pukkamustard")
  (copyright (2020))
  (synopsis "Guile library for the Semantic Web")
  (description
    "Guile Schemantic is a Guile library for the Semantic Web and implements the Resource Description Framework (RDF).")
  (home-page
    "https://gitlab.com/openengiadina/guile-schemantic")
  (license gpl3+)
  (dependencies `(("guile-rdf" ,guile-rdf)))
  (files (libraries
           ((scheme-file "schemantic")
            (directory
             "schemantic"
             ((directory
               "serialization"
               ((scheme-file "turtle")))
              (directory
               "graph"
               ((directory "vhash" ((scheme-file "index")))
                (scheme-file "vhash")))
              (directory
               "fragment-graph"
               ((scheme-file "radix-sort")
                (scheme-file "csexp")))
              (directory "interop" ((scheme-file "guile-rdf")))
              (directory
               "rdf"
               ((scheme-file "lang-string")
                (scheme-file "datalog")))
              (scheme-file "lvar")
              (scheme-file "iri")
              (scheme-file "fragment-graph")
              (scheme-file "xsd")
              (scheme-file "rdf")
              (scheme-file "literal")
              (scheme-file "blank-node")
              (scheme-file "ns")))
            (scheme-file "datalog")
            (directory "datalog"
                       ((scheme-file "relational-algebra")
                        (scheme-file "vhash-set")))))
         (tests ((directory
                  "tests"
                  ((directory "data" ())
                   (directory
                    "schemantic"
                    ((directory "graph" ((scheme-file "vhash")))
                     (directory "rdf" ((scheme-file "datalog")))
                     (directory
                      "serializaton"
                      ((scheme-file "turtle")))
                     (scheme-file "lvar")
                     (scheme-file "iri")
                     (scheme-file "fragment-graph")
                     (scheme-file "rdf")
                     (scheme-file "literal")))
                   (scheme-file "datalog")
                   (directory
                    "datalog"
                    ((scheme-file "relational-algebra")))))))
         (programs ((directory "scripts" ())))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "schemantic")))
            (text-file "NEWS")
            (text-file "AUTHORS")
            (text-file "ChangeLog")))
         (infrastructure
           ((scheme-file "guix")
            (scheme-file "hall")
            (directory
              "build-aux"
              ((scheme-file "test-driver")))
            (autoconf-file "configure")
            (automake-file "Makefile")
            (in-file "pre-inst-env")))))
