(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(package
  (name "guile-schemantic")
  (version "0.1")
  (source "./guile-schemantic-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
   `(("autoconf" ,autoconf)
     ("automake" ,automake)
     ("pkg-config" ,pkg-config)
     ("texinfo" ,texinfo)
     ("guile-hall" ,guile-hall)))
  (inputs `(("guile" ,guile-3.0)))
  (propagated-inputs `(("guile-rdf" ,guile-rdf)))
  (synopsis "Guile library for the Semantic Web")
  (description
    "Guile Schemantic is a Guile library for the Semantic Web and implements the Resource Description Framework (RDF).")
  (home-page
    "https://gitlab.com/openengiadina/guile-schemantic")
  (license license:gpl3+))

