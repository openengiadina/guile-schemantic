; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic blank-node)
  #:use-module (schemantic iri)

  #:use-module (oop goops)

  #:export (<blank-node>
            make-blank-node))

(define-class <blank-node> (<term>)
  (value #:init-keyword #:value #:getter blank-node-value))

(define-method (equal? (x <blank-node>) (y <blank-node>))
  (equal? (iri-value x) (iri-value y)))

(define-method (write (self <blank-node>) port)
  (format port "<blank-node ~a>" (blank-node-value self)))

(define (make-blank-node value)
  (make <blank-node> #:value value))
