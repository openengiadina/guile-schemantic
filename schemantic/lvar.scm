; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic lvar)
  #:use-module (oop goops)

  #:use-module (schemantic iri)

  #:use-module (ice-9 format)

  #:export (<lvar>
            lvar
            lvar?
            lvar-symbol))

;; Logical Variable

(define-class <lvar> (<term>)
  (symbol #:init-keyword #:symbol #:init-form (gensym "lvar") #:getter lvar-symbol))

(define-method (write (self <lvar>) port)
  (format port "?~a" (lvar-symbol self)))

(define-method (equal? (x <lvar>) (y <lvar>))
  (equal? (lvar-symbol x) (lvar-symbol y)))

(define lvar
  (case-lambda
    (() (make <lvar>))
    ((s) (make <lvar> #:symbol s))))

(define (lvar? x)
  (is-a? x <lvar>))
