; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic graph vhash index)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)
  #:use-module ((schemantic rdf) #:select (lvar?))
  #:export (index-add
            index-match
            index->list))

;; VHash based nested index.
;; Useful for defining indices for graph like structures

(define (vhash-ref index key default)
  (if (vhash-assoc key index)
      (cdr (vhash-assoc key index))
      default))

(define (index-add* index entries)
  (fold
   (lambda (entry index) (index-add index entry))
   index
   entries))

(define (index-add index entry)
  (match entry

    (() index)

    ((key)
     (vhash-cons key #t
                 (vhash-delete key index)))

    ((key . rest)
     (vhash-cons key
                 (index-add
                  (vhash-ref index key vlist-null) rest)
                 (vhash-delete key index)))))

(define (index-ref index key)
  (match key
    (() index)
    ((key)
     (vhash-ref index key vlist-null))
    ((key . rest)
     (index-ref
      (vhash-ref index key vlist-null)
      rest))))

(define (index-match index key)
  (match key
    (() (index->list index))

    ((k . rest)
     (if (lvar? k)

         ;; iterate over all keys in index, match and concat the result
         (vhash-fold
          (lambda (ki sub-index result)
            (append (index-match index (cons ki rest)) result))
          '()
          index)

         ;; k is concrete, match rest of key and add k to front of results
         (map (lambda (r) (cons k r))
              (index-match (vhash-ref index k vlist-null) rest))))))

;; (index-match
;;  (index-add* vlist-null '((1 2) (1 3) (2 2)))
;;  `(,(lvar) 3))

(define (index->list index)
  (cond
   ;; enumerate all elements in index
   ((vlist? index) (vlist-fold
                    (lambda (el lst)
                      (match el
                        ((key . ()) (cons (list key) lst))

                        ((key . sub-index)
                         (apply append
                                (map (lambda (sub-values) (cons key sub-values))
                                     (index->list sub-index))
                                (list lst)))))
                    '()
                    index))

   ;; if index is not a vlist this indicates the end of a single path. Return '(()) so that path can be reconstructed.
   (else '(()))))
