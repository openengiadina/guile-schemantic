; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic graph vhash)
  #:use-module (oop goops)

  #:use-module (schemantic rdf)
  #:use-module (schemantic graph vhash index)

  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)

  #:export (<vhash-graph>
            make-vhash-graph))

(define-class <vhash-graph> (<graph>)
  ;; NOTE more indices (ops and pso) could be added to increase performance of certain queries
  (spo #:accessor spo))

(define-method (initialize (g <vhash-graph>) initargs)
  (set! (spo g) vlist-null))

(define (make-vhash-graph)
  (make <vhash-graph>))

(define-method (graph-add (g <vhash-graph>)
                          (s <term>)
                          (p <term>)
                          (o <term>))
    ;; add to spo index
    (set! (spo g) (index-add (spo g) (list s p o)))
    ;; return graph to allow chaining of multiple adds
    g)

(define-method (graph-match (g <vhash-graph>) (s <term>) (p <term>) (o <term>))
  (map (match-lambda ((s p o) (make-triple s p o))) ; transform result when querying the index to a triple
       (index-match (spo g) (list s p o))))

