; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic ns)
  #:use-module (schemantic iri)
  #:export (rdf rdfs owl xsd))

(define-namespace rdf "http://www.w3.org/1999/02/22-rdf-syntax-ns#")
(define-namespace rdfs "http://www.w3.org/2000/01/rdf-schema#")
(define-namespace owl "http://www.w3.org/2002/07/owl#")
(define-namespace xsd "http://www.w3.org/2001/XMLSchema#")
