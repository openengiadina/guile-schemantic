; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic rdf)
  #:use-module (oop goops)

  #:use-module (schemantic iri)
  #:use-module (schemantic literal)
  #:use-module (schemantic lvar)
  #:use-module (schemantic xsd)
  #:use-module (schemantic rdf lang-string)

  #:use-module (ice-9 format)

  #:export (<triple>
            make-triple
            triple?
            triple-subject
            triple-predicate
            triple-object

            <graph>
            graph?
            graph-add
            graph-radd
            graph-match

            <graph-query>
            graph-query?
            graph-query)

  #:re-export (<term>

               <iri>
               make-iri
               iri?
               iri-value
               define-namespace

               <literal>
               make-literal
               literal?
               literal-value
               literal-lexical
               literal-canonical
               literal-datatype
               literal-language

               <lvar>
               lvar
               lvar?
               lvar-symbol

               <generic-literal>
               make-generic-literal

               <rdf:langString>
               make-lang-string))

;; Triple

(define-class <triple> ()
  (subject #:init-keyword #:s #:getter triple-subject)
  (predicate #:init-keyword #:p #:getter triple-predicate)
  (object #:init-keyword '#:o #:getter triple-object))

(define-method (write (self <triple>) port)
  (format port "<triple ~a ~a ~a>"
          (triple-subject self)
          (triple-predicate self)
          (triple-object self)))

(define-method (equal? (x <triple>) (y <triple>))
  (and (equal? (triple-subject x) (triple-subject y))
       (equal? (triple-object x) (triple-object y))
       (equal? (triple-predicate x) (triple-predicate y))))

(define-method (initialize (t <triple>) initargs)
  (next-method))

(define (make-triple s p o)
  (make <triple> #:s s #:p p #:o o))

(define (triple? x)
  (is-a? x <triple>))

;; Graph

;; A graph is an arbirtary  container of triples. It can be an in-memory structure or a connection to a database.

(define-class <graph> ())

(define (graph? x)
  (is-a? x <graph>))

(define-generic graph-add)

(define-method (graph-add (g <graph>) (t <triple>))
  (let ((s (triple-subject t))
        (p (triple-predicate t))
        (o (triple-object t)))
    (graph-add g s p o)))

(define-generic graph-match)

(define-method (graph-radd (g <graph>))
  "Returns a SRFI-171 reducer that adds triples to graph g"
  (case-lambda
    (() g)
    ((result) result)
    ((result triple) (graph-add g triple))))

(define-method (graph-match (g <graph>) (t <triple>))
  (graph-match g (triple-subject t) (triple-predicate t) (triple-object t)))

;; Graph Query

;; This is a helper to successively build up a query (in spo order). Usefulness
;; of this remains to be seen...

(define-class <graph-query> ()
  (graph #:init-keyword #:graph #:getter graph-query-graph)
  (s #:init-keyword #:s #:init-thunk lvar #:getter graph-query-s)
  (p #:init-keyword #:p #:init-thunk lvar #:getter graph-query-p)
  (o #:init-keyword #:o #:init-thunk lvar #:getter graph-query-o))

(define (graph-query? x)
  (is-a? x <graph-query>))

(define-method (write (self <graph-query>) port)
  (format port "<graph-query ~a ~a ~a ~a>"
          (graph-query-graph self)
          (graph-query-s self)
          (graph-query-p self)
          (graph-query-o self)))

(define-method (graph-query (graph <graph>) (s <iri>))
  (make <graph-query> #:graph graph #:s s))

(define-method (graph-query (q <graph-query>) (iri <iri>))
  "Build up a query with successive (graph-query) calls."
  (cond
   ((lvar? (graph-query-s q))
    (make <graph-query>
      #:graph (graph-query-graph q)
      #:s iri
      #:p (graph-query-p q)
      #:o (graph-query-o q)))

   ((lvar? (graph-query-p q))
    (make <graph-query>
      #:graph (graph-query-graph q)
      #:s (graph-query-s q)
      #:p iri
      #:o (graph-query-o q)))

   ((lvar? (graph-query-o q))
    (make <graph-query>
      #:graph (graph-query-graph q)
      #:s (graph-query-s q)
      #:p (graph-query-p q)
      #:o iri))

   ;; too far
   (else q)))

(define-method (graph-match (q <graph-query>))
  (graph-match (graph-query-graph q)
               (graph-query-s q)
               (graph-query-p q)
               (graph-query-o q)))
