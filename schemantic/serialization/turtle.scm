; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic serialization turtle)
  #:use-module (schemantic rdf)
  #:use-module (schemantic interop guile-rdf)

  #:use-module (turtle tordf)

  #:use-module (srfi srfi-171)

  #:export (turtle-transduce))

(define (turtle-transduce xform f string base-iri)
  (list-transduce
   (compose (tmap rdf-triple->triple) xform)
   f
   (turtle->rdf string (iri-value base-iri))))
