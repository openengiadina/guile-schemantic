; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic fragment-graph radix-sort)
  #:use-module (srfi srfi-1)
  #:use-module (rnrs bytevectors)
  #:export (radix-sort))


(define (max-length bvs)
  (fold
   (lambda (bv cmax) (max (bytevector-length bv) cmax))
   0 bvs))

(define (make-buckets)
  (make-array '() 256))

(define (collect-buckets buckets)
  (apply append (map reverse (array->list buckets))))

(define (clear-buckets buckets)
  (array-fill! buckets '()))

(define (to-bucket buckets v bv)
  (array-set! buckets
              (cons bv (array-ref buckets v))
              v))

(define (byte-ref bv i)
  (if (< i (u8vector-length bv))
      (u8vector-ref bv i) 0))

(define (radix-sort bvs)
  (let ((buckets (make-buckets)))
    (let loop ((i (max-length bvs))
               (bvs bvs))
      (if (<= 0 i)
          (begin
            ;; clear buckets from last iteration
            (clear-buckets buckets)
            ;; place bytevectors in buckets according to value at position i
            (for-each
             (lambda (bv) (to-bucket buckets (byte-ref bv i) bv)) bvs)
            ;; loop on next position
            (loop (1- i)
                  (collect-buckets buckets)))
          ;; done return the sorted bytevectors
          bvs))))

