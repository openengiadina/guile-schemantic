; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic fragment-graph csexp)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 exceptions)
  #:export (put-csexp
            get-csexp
            csexp->bytevector
            csexp->string
            bytevector->csexp))

;; Cannonical S-Expressions
;; See https://people.csail.mit.edu/rivest/Sexp.txt

;; Error handling

(define &csexp-parse-error
  (make-exception-type "csexp-parse-error"
                       &external-error
                       '()))

(define (make-csexp-parse-error msg)
  (make-exception
   ((record-constructor &csexp-parse-error))
   (make-exception-with-message msg)))

(define &csexp-encode-error
  (make-exception-type "csexp-encode-error"
                       &programming-error
                       '(expression)))

(define (make-csexp-encode-error expression)
   ((record-constructor &csexp-encode-error) expression))

;; Helpers for constants

(define open-parens-code
  (char->integer #\())

(define (open-parens-code? byte)
  (= byte open-parens-code))

(define close-parens-code
  (char->integer #\)))

(define (close-parens-code? byte)
  (= byte close-parens-code))

(define colon-code
  (char->integer #\:))

(define (colon-code? byte)
  (= byte colon-code))

(define (is-digit? byte)
  (if (number? byte)
      (char-numeric? (integer->char byte))
      #f))


;; Parse CSexp

(define (get-number port)
  "Read a decimal number from port"
  (let ((digits '()))

    (while (is-digit? (lookahead-u8 port))
      (set! digits (cons
                    (integer->char (get-u8 port))
                    digits)))

    (string->number
     (reverse-list->string digits))))

(define (expect-colon port)
  "Read a colon from port or raise an exception"
  (let ((byte (get-u8 port)))
    (if (colon-code? byte)
        #t
        (raise-continuable (make-csexp-parse-error "expecting colon")))))

(define (get-netstring port)
  "Get a netstring from the port"
  (let ((n (get-number port)))
    (if n
        (begin
          (expect-colon port)
          (get-bytevector-n port n))
        #f)))

(define (expect-open-parens port)
  "Read an opening parens from port or raise an exception"
  (let ((byte (get-u8 port)))
    (if (open-parens-code? byte)
        #t
        (raise-continuable (make-csexp-parse-error "expecting open parens")))))

(define (expect-close-parens port)
  "Read a closing parens from port or raise an exception"
  (let ((byte (get-u8 port)))
    (if (close-parens-code? byte)
        #t
        (raise-continuable (make-csexp-parse-error "expecting close parens")))))

(define (get-expression port)
  "Get an expression which is either a list of expressions or a bytevector"
  (let ((lookahead (lookahead-u8 port)))
    (cond

     ;; an opening parens -> get a list
     ((open-parens-code? lookahead) (get-list port))

     ;; a numeric value -> a bytevector
     ((is-digit? lookahead) (get-netstring port))

     ;; else something went wrong
     (raise-continuable
      (make-csexp-parse-error "expecting numeric value or open parens")))))

(define (get-list port)
  "Get a list of expressions"

  ;; initialize emtpy list
  (let ((lst '()))

    ;; get openeing parens
    (expect-open-parens port)

    ;; loop over port while lookahead is not closing parens
    (while (not (close-parens-code? (lookahead-u8 port)))

      ;; get the expression and add to list
      (set! lst (cons (get-expression port) lst)))

    ;; get closing parens
    (expect-close-parens port)

    ;; return list in proper order
    (reverse lst)))

;; Encoder

(define (put-netstring port bv)
  "Write bytevector to port as netstring"
  (put-bytevector port
                  (string->utf8
                   (string-append (number->string (bytevector-length bv)) ":")))
  (put-bytevector port bv))

(define (put-expression port exp)
  "Write encoded expression on port"
  (cond
   ((bytevector? exp) (put-netstring port exp))
   ((list? exp) (put-list port exp))
   ((string? exp) (put-expression port (string->utf8 exp)))
   ((symbol? exp) (put-expression port (symbol->string exp)))
   ((number? exp) (put-expression port (number->string exp)))
   (else (raise-continuable (make-csexp-encode-error exp)))))

(define (put-list port lst)
  "Write encoded list to port"
  (put-u8 port open-parens-code)
  (map (lambda (el) (put-expression port el)) lst)
  (put-u8 port close-parens-code))

;; Interface

(define (put-csexp port exp)
  "Write expression to port as cannonical s-expression"
  (put-expression port exp))

(define (get-csexp port)
  "Read cannonical s-expression from port"
  (get-expression port))

(define (csexp->bytevector exp)
  "Returns bytevector with expression encoded as csexp"
  (call-with-values
      (lambda () (open-bytevector-output-port))
    (lambda (port get-bytevector)
      (put-csexp port exp)
      (get-bytevector))))

(define (csexp->string exp)
  "Returns string with encoded csexp"
  (utf8->string (csexp->bytevector exp)))

(define (bytevector->csexp bv)
  "Read csexp from bytevector"
  (get-csexp (open-bytevector-input-port bv)))
