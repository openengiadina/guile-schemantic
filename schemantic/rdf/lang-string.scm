; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic rdf lang-string)
  #:use-module (oop goops)

  #:use-module (schemantic literal)
  #:use-module ((schemantic ns) #:select (rdf))

  #:export (<rdf:langString>
            make-lang-string))

(define-class <rdf:langString> (<literal>)
  (language #:init-keyword #:language #:getter literal-language))

(define-method (literal-datatype (l <rdf:langString>)) (rdf "langString"))
(define-method (literal-lexical (l <rdf:langString>)) (literal-value l))
(define-method (literal-canonical (l <rdf:langString>)) (literal-value l))

(define* (make-lang-string value #:key language)
  (make <rdf:langString> #:value value #:language language))

(define-method (write (self <rdf:langString>) port)
  (format port "<rdf:langString \"~a\"@~a>" (literal-value self) (literal-language self)))
