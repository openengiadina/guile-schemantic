(define-module (schemantic rdf datalog)
  #:use-module (schemantic rdf)
  #:use-module ((schemantic ns) #:select (rdf rdfs))

  #:use-module (datalog)
  #:use-module (datalog vhash-set)

  #:use-module (ice-9 vlist)

  #:use-module (srfi srfi-1)

  #:export (add-rdf-graph-to-edb

            rhodf))

(define (add-rdf-graph-to-edb predicate-symbol graph context)
  (vhash-cons
   predicate-symbol
   (lambda (attributes)
     (fold
      (λ (triple result)
        (set-adjoin
         result
         (vector (triple-subject triple)
                 (triple-predicate triple)
                 (triple-object triple))))
      vlist-null
      (graph-match graph
                   (vector-ref attributes 0)
                   (vector-ref attributes 1)
                   (vector-ref attributes 2))))
   context))


;; The ρdf fragment of RDF
;; See: Muñoz, S., Pérez, J., & Gutierrez, C. (2009). Simple and Efficient Minimal RDFS. Web Semantics: Science, Services and Agents on the World Wide Web, 7(3), 220–234. doi:10.1016/j.websem.2009.07.003
(define rhodf
  (list

   ;; Subproperty (a)
   (:- (rhodf (lvar 'a) (rdfs "subProperty") (lvar 'c))
    (rhodf (lvar 'a) (rdfs "subProperty") (lvar 'b))
    (rhodf (lvar 'b) (rdfs "subProperty") (lvar 'c)))

   ;; Subproperty (b)
   (:- (rhodf (lvar 'x) (lvar 'b) (lvar 'y))
    (rhodf (lvar 'a) (rdfs "subProperty") (lvar 'b))
    (rhodf (lvar 'x) (lvar 'a) (lvar 'y)))

   ;; Subclass (a)
   (:- (rhodf (lvar 'a) (rdfs "subClass") (lvar 'c))
    (rhodf (lvar 'a) (rdfs "subClass") (lvar 'b))
    (rhodf (lvar 'b) (rdfs "subClass") (lvar 'c)))

   ;; Subclass (b)
   (:- (rhodf (lvar 'x) (rdf "type") (lvar 'b))
    (rhodf (lvar 'a) (rdfs "subClass") (lvar 'b))
    (rhodf (lvar 'x) (rdf "type") (lvar 'a)))

   ;; Typing (a)
   (:- (rhodf (lvar 'x) (rdf "type") (lvar 'b))
    (rhodf (lvar 'a) (rdfs "domain") (lvar 'b))
    (rhodf (lvar 'x) (lvar 'a) (lvar 'y)))

   ;; Typing (b)
   (:- (rhodf (lvar 'y) (rdf "type") (lvar 'b))
    (rhodf (lvar 'a) (rdfs "range") (lvar 'b))
    (rhodf (lvar 'x) (lvar 'a) (lvar 'y)))

   ;; Implicit Typing (a)
   (:- (rhodf (lvar 'x) (rdf "type") (lvar 'b))
    (rhodf (lvar 'a) (rdfs "domain") (lvar 'b))
    (rhodf (lvar 'c) (rdfs "subProperty") (lvar 'a))
    (rhodf (lvar 'x) (lvar 'c) (lvar 'y)))

   ;; Implicit Typing (b)
   (:- (rhodf (lvar 'y) (rdf "type") (lvar 'b))
    (rhodf (lvar 'a) (rdfs "range") (lvar 'b))
    (rhodf (lvar 'c) (rdfs "subProperty") (lvar 'a))
    (rhodf (lvar 'x) (lvar 'c) (lvar 'y)))))
