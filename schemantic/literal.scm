; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic literal)
  #:use-module (oop goops)

  #:use-module (schemantic iri)

  #:export (<literal>
            literal?
            make-literal
            literal-value
            literal-lexical
            literal-canonical
            literal-datatype
            literal-language

            <generic-literal>
            make-generic-literal))

(define-class <literal> (<term>)
  (value #:init-keyword #:value #:getter literal-value))

(define-method (write (self <literal>) port)
  (format port "<literal ~a ~a>" (literal-value self) (literal-datatype self)))

(define (literal? x)
  (is-a? x <literal>))

(define-generic make-literal)
(define-method (make-literal (l <literal>)) l)

(define-generic literal-lexical)
(define-generic literal-canonical)
(define-generic literal-datatype)

(define-generic literal-language)
(define-method (literal-language (l <literal>)) #f)

(define-method (equal? (x <literal>) (y <literal>))
  (and (equal? (literal-value x) (literal-value y))
       (equal? (literal-datatype x) (literal-datatype y))
       (equal? (literal-language x) (literal-language y))))

;; Generic literal

(define-class <generic-literal> (<literal>)
  (datatype #:init-keyword #:datatype))

(define-method (literal-datatype (l <generic-literal>)) (slot-ref l 'datatype))

(define* (make-generic-literal value #:key datatype)
  (if (iri? datatype)
      (make <generic-literal> #:value value #:datatype datatype)
      #f))
