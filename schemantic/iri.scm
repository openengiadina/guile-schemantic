; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic iri)
  #:use-module (oop goops)
  #:use-module (oop goops describe)

  #:use-module (web uri)

  #:use-module (ice-9 optargs)
  #:use-module (ice-9 exceptions)

  #:export (<term>
            <iri>
            make-iri
            iri?
            iri-value
            define-namespace))

(define-class <term> ())

(define-class <iri> (<term>)
  (value #:init-keyword #:value #:getter iri-value))

(define-method (equal? (x <iri>) (y <iri>))
  (equal? (iri-value x) (iri-value y)))

(define-method (write (self <iri>) port)
  (format port "<iri ~a>" (iri-value self)))

(define-generic make-iri)
(define-method (make-iri (s <string>)) (make <iri> #:value s))
(define-method (make-iri (iri <iri>)) iri)
(define-method (make-iri (x <top>))
  (if (uri? x)
      (make-iri (uri->string x))
      (no-applicable-method)))

(define (iri? x)
  "Returns true if x is an iri"
  (is-a? x <iri>))

;; Syntax for defining new namespace

(define-syntax-rule (define-namespace name uri)
  (define (name id)
    (make-iri (string-append uri id))))

