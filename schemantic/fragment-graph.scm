; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic fragment-graph)
  #:use-module (schemantic rdf)
  #:use-module (schemantic fragment-graph csexp)
  #:use-module (schemantic fragment-graph radix-sort)
  #:use-module (schemantic graph vhash index)

  #:use-module (oop goops)

  #:use-module (web uri)

  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (ice-9 vlist)

  #:export (<fragment-graph>
            fragment-graph?
            make-fragment-graph
            fragment-graph-base-subject
            fragment-graph->csexp

            <fragment-reference>
            fragment-reference?
            make-fragment-reference
            term->fragment-reference))

;; Fragment Graph

(define-class <fragment-graph> (<graph>)
  (base-subject #:init-keyword #:base-subject #:accessor fragment-graph-base-subject)
  (statements #:init-value vlist-null #:accessor statements)
  (fragement-statements #:init-value vlist-null #:accessor fragment-statements))

(define-method (write (self <fragment-graph>) port)
  (format port "<fragment-graph ~a ~x>" (fragment-graph-base-subject self) (object-address self)))

(define (make-fragment-graph base-subject)
  (make <fragment-graph> #:base-subject base-subject))

(define (fragment-graph? x)
  (is-a? x <fragment-graph>))

;; Fragment Reference

(define-class <fragment-reference> (<term>)
  (id #:init-keyword #:id #:getter fragment-reference-id))

(define-method (equal? (x <fragment-reference>) (y <fragment-reference>))
  (equal? (fragment-reference-id x) (fragment-reference-id y)))

(define-method (write (self <fragment-reference>) port)
  (format port "<fragment-reference ~a>" (fragment-reference-id self)))

(define (fragment-reference? x)
  (is-a? x <fragment-reference>))

(define-method (make-fragment-reference id)
  (make <fragment-reference> #:id id))

(define (term->fragment-reference base-subject term)
  (if (iri? term)
      (let* ((uri (string->uri (iri-value term)))
             (uri-without-fragment (build-uri (uri-scheme uri)
                                              #:userinfo (uri-userinfo uri)
                                              #:host (uri-host uri)
                                              #:port (uri-port uri)
                                              #:path (uri-path uri)
                                              #:query (uri-query uri))))
        (if (equal? (iri-value base-subject) (uri->string uri-without-fragment))
            (make-fragment-reference (uri-fragment uri))

            ;; not a fragment of base-subject, return as is
            term))

      ;; else just return the term
      term))

;; (term->fragment-reference (make-iri "https://example.com/")
;;                           (make-iri "https://example.com/#hello"))

;; (term->fragment-reference (make-iri "https://example.com")
;;                           (make-literal "hi"))
;;

(define (fragment-reference->term base-subject fragment-reference)
  (if (fragment-reference? fragment-reference)
      (let* ((base-subject-uri (string->uri (iri-value base-subject)))
             (uri (build-uri (uri-scheme base-subject-uri)
                             #:userinfo (uri-userinfo base-subject-uri)
                             #:host (uri-host base-subject-uri)
                             #:port (uri-port base-subject-uri)
                             #:path (uri-path base-subject-uri)
                             #:query (uri-query base-subject-uri)
                             #:fragment (fragment-reference-id fragment-reference))))
        (make-iri (uri->string uri)))

      fragment-reference))

;; add a statement
(define-method (graph-add (fg <fragment-graph>) (p <term>) (o <term>))
  (set! (statements fg)
    (index-add (statements fg)
               (list (term->fragment-reference (fragment-graph-base-subject fg) p)
                     (term->fragment-reference (fragment-graph-base-subject fg) o))))
  fg)

;; add a fragment statement
(define-method (graph-add (fg <fragment-graph>)
                          (fr <fragment-reference>)
                          (p <term>)
                          (o <term>))
  (set! (fragment-statements fg)
    (index-add (fragment-statements fg)
               (list fr
                     (term->fragment-reference (fragment-graph-base-subject fg) p)
                     (term->fragment-reference (fragment-graph-base-subject fg) o))))
  fg)

;; catch all method that decides if triple should be added as statement or fragment-statement
(define-method (graph-add (fg <fragment-graph>) (s <term>) (p <term>) (o <term>))
  (let ((fragment-reference (term->fragment-reference (fragment-graph-base-subject fg) s)))
    (cond
     ;; not a fragment of base-subject
     ((not fragment-reference) fg)

     ((fragment-reference? fragment-reference)
      (if (not (fragment-reference-id fragment-reference))
          ;; add as statement
          (graph-add fg p o)
          ;; add as fragment-statement
          (graph-add fg fragment-reference p o)))))
  fg)

(define-method (graph-match (fg <fragment-graph>) (s <term>) (p <term>) (o <term>))
  (let ((fragment-reference (term->fragment-reference (fragment-graph-base-subject fg) s))
        (base-subject (fragment-graph-base-subject fg)))
    (cond
     ;; s is not base-subject or fragment, return empty list
     ((not fragment-reference) '())

     ;; subject is a lvar match all statements and fragment statements
     ((lvar? fragment-reference)
      (append
       (map (match-lambda ((p o) (make-triple
                                  base-subject
                                  (fragment-reference->term base-subject p)
                                  (fragment-reference->term base-subject o))))
            (index-match (statements fg) (list (term->fragment-reference base-subject p)
                                               (term->fragment-reference base-subject o))))
       (map (match-lambda ((fr p o) (make-triple
                                     (fragment-reference->term base-subject fr)
                                     (fragment-reference->term base-subject p)
                                     (fragment-reference->term base-subject o))))
            (index-match (fragment-statements fg) (list fragment-reference
                                                        (term->fragment-reference base-subject p)
                                                        (term->fragment-reference base-subject o))))))

     ((fragment-reference? fragment-reference)
      (if (not (fragment-reference-id fragment-reference))
          ;; s is base subject, only match statements
          (map (match-lambda ((p o) (make-triple
                                     base-subject
                                     (fragment-reference->term base-subject p)
                                     (fragment-reference->term base-subject o))))
               (index-match (statements fg) (list (term->fragment-reference base-subject p)
                                                  (term->fragment-reference base-subject o))))

          ;; s is a proper fragment-reference
          (map (match-lambda ((fr p o) (make-triple
                                        (fragment-reference->term base-subject fr)
                                        (fragment-reference->term base-subject p)
                                        (fragment-reference->term base-subject o))))
               (index-match (fragment-statements fg) (list fragment-reference
                                                           (term->fragment-reference base-subject p)
                                                           (term->fragment-reference base-subject o)))))))))

;; Helpers to build graph-queries on fragment graph
;;
;; If given an <iri> to build initial graph-query, it is assumed that the query
;; should be done for the base subject.
;;
;; Only if a <fragment-reference> is given as initial argument it will be put in
;; subject position.

(define-method (graph-query (fg <fragment-graph>) (s <iri>))
  (make <graph-query>
    #:graph fg
    #:s (make-fragment-reference #f)
    #:p (term->fragment-reference (fragment-graph-base-subject fg) s)))

(define-method (graph-query (fg <fragment-graph>) (fr <fragment-reference>))
  (make <graph-query> #:graph fg #:s fr))

;; CSexp encoding

(define (term->csexp term)
  (cond

   ((iri? term) (iri-value term))

   ((fragment-reference? term) `(f ,(fragment-reference-id term)))

   ((is-a? term <rdf:langString>)
    `(l ,(literal-canonical term)
        ,(term->csexp (literal-datatype term))
        ,(literal-language term)))

   ((literal? term) `(l ,(literal-canonical term) ,(term->csexp (literal-datatype term))))))

(define (fragment-graph->csexp fg)
  (let* ((statements
          (map (match-lambda ((p o)
                              `(s ,(term->csexp p) ,(term->csexp o))))
               (index-match (statements fg) (list (lvar) (lvar)))))
         (fragment-statements
          (map (match-lambda ((f p o)
                              `(fs ,(term->csexp f)
                                  ,(term->csexp p)
                                  ,(term->csexp o))))
               (index-match (fragment-statements fg)
                            (list (lvar) (lvar) (lvar)))))
         (sorted-statements
          (map bytevector->csexp
               (radix-sort
                (map csexp->bytevector
                     (append statements fragment-statements))))))

    (csexp->bytevector
     (cons 'rdf sorted-statements))))
