; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic interop guile-rdf)
  #:use-module (schemantic rdf)
  #:use-module (schemantic blank-node)
  #:use-module ((schemantic ns) #:select (xsd rdf))

  #:use-module (rdf rdf)

  #:export (rdf-triple->triple))

;; Interop to the guile-rdf library

;; NOTE maybe it makes sense to keep the mapping of datatype+lexical-form to
;; literal in some other place as this is something that would be used by other
;; serializations... literals are tricky
(define (rdf-literal->literal literal)
  (let ((datatype (make-iri (rdf-literal-type literal))))
    (cond

     ((equal? (rdf "langString") datatype)
      (make-lang-string
       (rdf-literal-lexical-form literal)
       #:language (rdf-literal-langtag literal)))

     ((equal? (xsd "string") datatype)
      (make-literal (rdf-literal-lexical-form literal)))

     ((equal? (xsd "integer") datatype)
      (make-literal (string->number (rdf-literal-lexical-form literal))))

     (else (make-generic-literal (rdf-literal-lexical-form literal)
                                 #:datatype datatype)))))

(define (rdf-term->term term)
  (cond
   ((string? term) (make-iri term))
   ((integer? term) (make-blank-node term))
   ((rdf-literal? term) (rdf-literal->literal term))))

(define (rdf-triple->triple t)
  (make-triple
   (rdf-term->term (rdf-triple-subject t))
   (rdf-term->term (rdf-triple-predicate t))
   (rdf-term->term (rdf-triple-object t))))
