; SPDX-FileCopyrightText: 2020 pukkamustard <pukkamustard@posteo.net>
;
; SPDX-License-Identifier: GPL-3.0-or-later

(define-module (schemantic xsd)
  #:use-module (oop goops)

  #:use-module (schemantic iri)
  #:use-module (schemantic literal)
  #:use-module ((schemantic ns) #:select (xsd))

  #:export (<xsd:string>
            <xsd:integer>))

(define-class <xsd:string> (<literal>))
(define-method (literal-lexical (x <xsd:string>)) (literal-value x))
(define-method (literal-canonical (x <xsd:string>)) (literal-value x))
(define-method (literal-datatype (x <xsd:string>)) (xsd "string"))
(define-method (write (self <xsd:string>) port) (format port "<xsd:string ~a>" (literal-value self)))
(define-method (make-literal (s <string>)) (make <xsd:string> #:value s))

(define-class <xsd:integer> (<literal>))
(define-method (literal-lexical (x <xsd:integer>)) (number->string (literal-value x)))
(define-method (literal-canonical (x <xsd:integer>)) (number->string (literal-value x)))
(define-method (literal-datatype (x <xsd:integer>)) (xsd "integer"))
(define-method (write (self <xsd:integer>) port) (format port "<xsd:integer ~a>" (literal-value self)))
(define-method (make-literal (s <integer>)) (make <xsd:integer> #:value s))

;; TODO add the other XSD datatypes
