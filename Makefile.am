

bin_SCRIPTS = 

# Handle substitution of fully-expanded Autoconf variables.
do_subst = $(SED)					\
  -e 's,[@]GUILE[@],$(GUILE),g'				\
  -e 's,[@]guilemoduledir[@],$(guilemoduledir),g'	\
  -e 's,[@]guileobjectdir[@],$(guileobjectdir),g'	\
  -e 's,[@]localedir[@],$(localedir),g'

nodist_noinst_SCRIPTS = pre-inst-env

GOBJECTS = $(SOURCES:%.scm=%.go)

moddir=$(prefix)/share/guile/site/$(GUILE_EFFECTIVE_VERSION)
godir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache
ccachedir=$(libdir)/guile/$(GUILE_EFFECTIVE_VERSION)/site-ccache

nobase_mod_DATA = $(SOURCES) $(NOCOMP_SOURCES)
nobase_go_DATA = $(GOBJECTS)

# Make sure source files are installed first, so that the mtime of
# installed compiled files is greater than that of installed source
# files.  See
# <http://lists.gnu.org/archive/html/guile-devel/2010-07/msg00125.html>
# for details.
guile_install_go_files = install-nobase_goDATA
$(guile_install_go_files): install-nobase_modDATA

EXTRA_DIST = $(SOURCES) $(NOCOMP_SOURCES)
GUILE_WARNINGS = -Wunbound-variable -Warity-mismatch -Wformat
SUFFIXES = .scm .go
.scm.go:
	$(AM_V_GEN)$(top_builddir)/pre-inst-env $(GUILE_TOOLS) compile $(GUILE_WARNINGS) -o "$@" "$<"

SOURCES = schemantic.scm \
          schemantic/serialization/turtle.scm \
          schemantic/graph/vhash/index.scm \
          schemantic/graph/vhash.scm \
          schemantic/fragment-graph/radix-sort.scm \
          schemantic/fragment-graph/csexp.scm \
          schemantic/interop/guile-rdf.scm \
          schemantic/rdf/lang-string.scm \
          schemantic/rdf/datalog.scm \
          schemantic/lvar.scm \
          schemantic/iri.scm \
          schemantic/fragment-graph.scm \
          schemantic/xsd.scm \
          schemantic/rdf.scm \
          schemantic/literal.scm \
          schemantic/blank-node.scm \
          schemantic/ns.scm \
          datalog.scm \
          datalog/relational-algebra.scm \
          datalog/vhash-set.scm

TESTS = tests/schemantic/graph/vhash.scm \
        tests/schemantic/rdf/datalog.scm \
        tests/schemantic/serializaton/turtle.scm \
        tests/schemantic/lvar.scm \
        tests/schemantic/iri.scm \
        tests/schemantic/fragment-graph.scm \
        tests/schemantic/rdf.scm \
        tests/schemantic/literal.scm \
        tests/datalog.scm \
        tests/datalog/relational-algebra.scm

TEST_EXTENSIONS = .scm
SCM_LOG_DRIVER =                                \
  $(top_builddir)/pre-inst-env                  \
  $(GUILE) --no-auto-compile -e main            \
      $(top_srcdir)/build-aux/test-driver.scm

# Tell 'build-aux/test-driver.scm' to display only source file names,
# not indivdual test names.
AM_SCM_LOG_DRIVER_FLAGS = --brief=yes

AM_SCM_LOG_FLAGS = --no-auto-compile -L "$(top_srcdir)"

AM_TESTS_ENVIRONMENT = abs_top_srcdir="$(abs_top_srcdir)"

info_TEXINFOS = doc/schemantic.texi
dvi: # Don't build dvi docs

EXTRA_DIST += README.org \
              README \
              HACKING \
              COPYING \
              NEWS \
              AUTHORS \
              ChangeLog \
              guix.scm \
              hall.scm \
              build-aux/test-driver.scm \
              configure.ac \
              Makefile.am \
              pre-inst-env.in \
              build-aux/test-driver.scm \
              $(TESTS)

ACLOCAL_AMFLAGS = -I m4

clean-go:
	-$(RM) $(GOBJECTS)
.PHONY: clean-go

CLEANFILES =					\
  $(GOBJECTS)					\
  $(TESTS:tests/%.scm=%.log)
